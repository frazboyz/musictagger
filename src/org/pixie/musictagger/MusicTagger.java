package org.pixie.musictagger;

import java.io.*;
import java.util.*;

/**
 * Created by Francis on 15/03/14.
 */
public class MusicTagger {

    public static final List<Pair<File, File>> files = new ArrayList<>();

    public static void main(final String[] args) throws IOException {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the directory which contains all your mp3 audio files");
        search(new File(reader.readLine()));
        System.out.println("Loaded " + files.size() + " audio files ready to tag");
        System.out.println("Do you want to tag these audio files? 'Y' to convert 'N' to exit");
        switch (reader.readLine()) {
            case "Y":
            case "y":
                for (final Pair<File, File> pair : files) {
                    final FileBuffer buffer = new FileBuffer(pair.b);
                    MP3Buffer.COMMENT.write(buffer, "Tagged By MusicTagger");
                    MP3Buffer.ALBUM.write(buffer, pair.a.getName());
                    MP3Buffer.ARTIST.write(buffer, pair.a.getName());
                }
                break;
            case "N":
            case "n":
                break;
        }
    }

    /**
     * Recursively searches down a file tree
     *
     * @param parent the parent or root folder to search inside
     */
    public static void search(final File parent) {
        for (final File file : parent.listFiles()) {
            if (file.canRead()) {
                if (file.isDirectory()) {
                    search(file);
                } else {
                    if (file.getName().contains(".mp3")) {
                        files.add(new Pair<>(parent, file));
                    } else {
                        System.out.println(file.getName() + " Uncompatible audio format");
                    }
                }
            } else {
                System.out.println(file.getAbsoluteFile() + " Permission Denied");
            }
        }
    }

}
