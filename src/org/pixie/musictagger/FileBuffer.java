package org.pixie.musictagger;

import java.io.*;

/**
 * Created by Francis on 15/03/14.
 */
public class FileBuffer {

    public final File file;

    public FileBuffer(final File file) {
        this.file = file;
    }

    public byte[] read() {
        try {
            final FileInputStream stream = new FileInputStream(file);
            final byte[] bytes = new byte[(int) file.length()];
            if (stream.read(bytes) == -1) {
                throw new RuntimeException("Failed to read file");
            }
            stream.close();
            return bytes;
        } catch (final IOException e) {
            throw new RuntimeException("Failed to read file : " + e.getMessage());
        }
    }

    public byte[] read(int start, int end) {
        int offset = 0;
        final byte[] raw = read();
        final byte[] bytes = new byte[end - start];
        for (int i = start; i < end; i++) {
            bytes[offset++] = raw[i];
        }
        return bytes;
    }

    public boolean write(final byte[] bytes) {
        try {
            final FileOutputStream stream = new FileOutputStream(file);
            stream.write(bytes);
            stream.close();
            return true;
        } catch (final IOException e) {
            throw new RuntimeException("Failed to write file : " + e.getMessage());
        }
    }

    public boolean write(final byte[] insert, final int start, final int end) {
        int offset = 0;
        final byte[] bytes = read();
        for (int i = start; i < end; i++) {
            bytes[i] = insert.length - 1 < offset ? 0 : insert[offset++];
        }
        return write(bytes);
    }

    public int getSize() {
        return (int) file.length();
    }
}
