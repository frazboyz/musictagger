package org.pixie.musictagger;

/**
 * Created by Francis on 15/03/14.
 */
public enum MP3Buffer {

    TAG(0, 2), NAME(3, 33), ARTIST(33, 62), ALBUM(63, 92), YEAR(93, 96), COMMENT(97, 126), GENRE(127, 128);

    /**
     * The byte offset positions in a MP3 file
     */
    private final int[] offsets;

    private MP3Buffer(final int... offsets) {
        this.offsets = offsets;
    }

    /**
     * @return the data the mp3 contains
     */
    public String read(final FileBuffer buffer) {
        final StringBuilder builder = new StringBuilder();
        final byte[] bytes = buffer.read((buffer.getSize() - 128) + offsets[0], (buffer.getSize() - 128) + offsets[1]);
        for (final byte b : bytes) {
            builder.append((char) (b & 0xFF));
        }
        return builder.toString();
    }

    /**
     * @param data the data you wish to wright
     */
    public void write(final FileBuffer buffer, final String data) {
        if (data.length() > (offsets[1] - offsets[0])) {
            throw new RuntimeException("Data is exceeding the maximum size(" + (offsets[1] - offsets[0]) + ")");
        }
        buffer.write(data.getBytes(), (buffer.getSize() - 128) + offsets[0], (buffer.getSize() - 128) + offsets[1]);
    }
}
