package org.pixie.musictagger;

/**
 * Created by Francis on 15/03/14.
 */
public class Pair<A, B> {

    public A a;
    public B b;

    public Pair(final A a, final B b) {
        this.a = a;
        this.b = b;
    }
}
